package com.walter.config;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author ：ch
 * @date ：Created in 2019/4/26
 * @description：
 * @modified By：
 * @version: 1.0
 */
@EnableTransactionManagement
@Configuration
@MapperScan("cn.walter.mapper*")
public class MybatisPlusConfig {
    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * @Description : mybatis-plus SQL执行效率插件【生产环境可以关闭】
     */
    @Bean  //console会打印执行的sql和时间
    public PerformanceInterceptor performanceInterceptor() {
        return new PerformanceInterceptor();
    }
}
