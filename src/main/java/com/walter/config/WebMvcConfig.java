package com.walter.config;

import com.walter.interceptor.ApiLogInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author ：ch
 * @date ：Created in 2019/4/26
 * @description：
 * @modified By：
 * @version: 1.0
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ApiLogInterceptor()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }
}
