package com.walter.exception;

/**
 * @author ：ch
 * @date ：Created in 2018/8/9
 * @description：
 * @modified By：
 * @version: 1.0
 */
public class LogicException extends RuntimeException {
    private String message;

    public LogicException(String message){
        super(message);
    }
}
