package com.walter.common;

/**
 * @author ：ch
 * @date ：Created in 2021/5/25
 * @description：
 * @modified By：
 * @version: 1.0
 */
public class BaseController {
    public <T> Result success(T data){
        return Result.success(data);
    }

    public Result success(){
        return Result.success();
    }

    public Result fail(String msg){
        return Result.fail(msg);
    }
}
