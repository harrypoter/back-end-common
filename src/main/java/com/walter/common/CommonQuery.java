package com.walter.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Setter;

import java.util.Optional;

/**
 * @author ：ch
 * @date ：Created in 2018/8/10
 * @description：
 * @modified By：
 * @version: 1.0
 */
@Setter
public class CommonQuery{
    @ApiModelProperty("一般用于模糊查询的参数")
    private String text;

    @ApiModelProperty("分页参数")
    private PageQuery page;
    //private String asc; //todo


    public String getText() {
        return text;
    }

    public PageQuery getPage() {
        return Optional.ofNullable(page)
                .orElseGet(PageQuery :: new);
    }

}
