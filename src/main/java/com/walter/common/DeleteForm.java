package com.walter.common;

import lombok.Data;

import java.util.List;

/**
 * dynamic-table Created by ch on 2018-08-16.
 * FOR: （通用）批量删除表单
 */
@Data
public class DeleteForm {
    private List<Object> ids;
}
