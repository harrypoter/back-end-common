package com.walter.common;

import com.baomidou.mybatisplus.plugins.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author ：ch
 * @date ：Created in 2018/8/16
 * @description：
 * @modified By：
 * @version: 1.0
 */
@Data
@Accessors(chain = true)
public class PageVO<T>  {
    @ApiModelProperty("记录总数")
    private Long total;

    @ApiModelProperty("记录总页数")
    private Long pages;

    @ApiModelProperty("页码")
    private Integer pageNum;

    @ApiModelProperty("每页大小")
    private Integer pageSize;

    @ApiModelProperty("记录列表")
    private List<T> records;

    public PageVO (Page<T> page){
        if (null != page){
            this.total = page.getTotal();
            this.pages = page.getPages();
            this.pageNum = page.getCurrent();
            this.pageSize = page.getSize();
            this.records = page.getRecords();
        }
    }
}
