package com.walter.common;

import io.swagger.annotations.ApiModelProperty;

import java.util.Optional;

/**
 * @author ：ch
 * @date ：Created in 2018/8/16
 * @description：
 * @modified By：
 * @version: 1.0
 */
public class PageQuery {
    @ApiModelProperty("页码")
    private Integer pageNum;

    @ApiModelProperty("每页大小")
    private Integer pageSize;

    public Integer getPageNum() {
        return Optional.ofNullable(pageNum)
                .orElse(1);
    }

    public Integer getPageSize() {
        return Optional.ofNullable(pageSize)
                .orElse(10);
    }

    public PageQuery setPageNum(Integer pageNum) {
        if (null == pageNum)
            this.pageNum = 1;
        else
            this.pageNum = pageNum;
        return this;
    }

    public PageQuery setPageSize(Integer pageSize) {
        if (null == pageSize)
            this.pageSize = 10;
        else
            this.pageSize = pageSize;
        return this;
    }
}
