package com.walter.common;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author ：ch
 * @date ：Created in 2018/8/14
 * @description：
 * @modified By：
 * @version: 1.0
 */
@Data
@Accessors(chain = true)
public class Result<T> {
    private Integer code;
    private String msg;
    private T data;

    public static <T>Result<T> success(T data){
        return new Result<T>().setCode(0).setData(data).setMsg("成功");
    }

    public static Result success(){
        return new Result().setCode(0).setData("success").setMsg("成功");
    }

    public static <T>Result<T> fail(String msg){
        return new Result<T>().setCode(-1).setMsg(msg);//todo errorCodeEnums
    }
}
