package com.walter.handler;

import com.walter.common.Result;
import com.walter.exception.LogicException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ch on 2019-04-26.
 * FOR:
 */
@Slf4j
@ControllerAdvice
public class ExceptionAspect {
    @ExceptionHandler(value = LogicException.class)
    @ResponseBody
    public Result handleLogicException(LogicException e){
        log.error("LogicException occur : {}",e.getMessage());
        return Result.fail(e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handleException(Exception e){
        log.error("unknown exception occur : {}",e.getMessage());
        return Result.fail("未知异常，请联系系统管理员");
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @ResponseBody
    public Result handleHttpMessageNotReadableException(HttpMessageNotReadableException e){
        log.error("LogicException occur : {}",e.getMessage());
        return Result.fail("提交的JSON数据格式不对,请校验后再提交!");
    }
}
