package com.walter.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ：ch
 * @date ：Created in 2018/8/9
 * @description： from jFinal
 * @modified By：
 * @version: 1.0
 */
public class Kv extends HashMap {
    private static final String STATUS_OK = "isOk";
    private static final String STATUS_FAIL = "isFail";

    public Kv() {
    }

    public static Kv by(Object key, Object value) {
        return (new Kv()).set(key, value);
    }

    public static Kv create() {
        return new Kv();
    }

    public static Kv ok() {
        return (new Kv()).setOk();
    }

    public static Kv ok(Object key, Object value) {
        return ok().set(key, value);
    }

    public static Kv fail() {
        return (new Kv()).setFail();
    }

    public static Kv fail(Object key, Object value) {
        return fail().set(key, value);
    }

    public Kv setOk() {
        super.put("isOk", Boolean.TRUE);
        super.put("isFail", Boolean.FALSE);
        return this;
    }

    public Kv setFail() {
        super.put("isOk", Boolean.FALSE);
        super.put("isFail", Boolean.TRUE);
        return this;
    }

    public boolean isOk() {
        Boolean isOk = (Boolean)this.get("isOk");
        return isOk != null && isOk;
    }

    public boolean isFail() {
        Boolean isFail = (Boolean)this.get("isFail");
        return isFail != null && isFail;
    }

    public Kv set(Object key, Object value) {
        super.put(key, value);
        return this;
    }

    public Kv set(Map map) {
        super.putAll(map);
        return this;
    }

    public Kv set(Kv kv) {
        super.putAll(kv);
        return this;
    }

    public Kv delete(Object key) {
        super.remove(key);
        return this;
    }

    public <T> T getAs(Object key) {
        return (T)this.get(key);
    }

    public String getStr(Object key) {
        return (String)this.get(key);
    }

    public Integer getInt(Object key) {
        return (Integer)this.get(key);
    }

    public Long getLong(Object key) {
        return (Long)this.get(key);
    }

    public Boolean getBoolean(Object key) {
        return (Boolean)this.get(key);
    }

    public boolean notNull(Object key) {
        return this.get(key) != null;
    }

    public boolean isNull(Object key) {
        return this.get(key) == null;
    }

    public boolean isTrue(Object key) {
        Object value = this.get(key);
        return value instanceof Boolean && (Boolean)value;
    }

    public boolean isFalse(Object key) {
        Object value = this.get(key);
        return value instanceof Boolean && !(Boolean)value;
    }

    public boolean equals(Object kv) {
        return kv instanceof Kv && super.equals(kv);
    }
}
