package com.walter.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：ch
 * @date ：Created in 2019/4/26
 * @description：
 * @modified By：
 * @version: 1.0
 */
@Slf4j
public class ApiLogInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{
        String apiPath = request.getRequestURI();
        String requestMethod= request.getMethod();
        String queryString= request.getQueryString();
        String contentType = request.getContentType();
        log.info("--------------Api Info Begin--------------");
        log.info("api : {}",apiPath);
        log.info("method : {}",requestMethod);
        if (!StringUtils.isEmpty(queryString))
            log.info("queryString : {}",queryString);
        if (!StringUtils.isEmpty(contentType))
            log.info("contentType : {}",contentType);
        log.info("--------------Api Info End--------------");
        return true;
    }

}
